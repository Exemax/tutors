module.exports = {
  env: {
    browser: true,
    es2020: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 11,
    sourceType: 'module',
  },
  plugins: [
    'react',
    '@typescript-eslint',
  ],
  rules: {
    'import/prefer-default-export': 0,
    'import/no-extraneous-dependencies': 0,
    'react/jsx-filename-extension': 0,
    // TODO Fix it:w
    'import/no-unresolved': 0,
    'no-underscore-dangle': 1,
    'import/extensions': 0,
    'global-require': 0,
    '@typescript-eslint/no-unused-vars': 1,
    'arrow-body-style': 0,
    'linebreak-style': ['error', process.platform === 'win32' ? 'windows' : 'unix'],
    'no-param-reassign': 0,
  },
};
