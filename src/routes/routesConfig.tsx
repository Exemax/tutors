import { FunctionComponent } from 'react';
import { RouteComponentProps } from 'react-router';
import { MainPage } from '../MainPage/MainPageApp';

export interface RoutesConfigTypes {
  path: string
  component: FunctionComponent<RouteComponentProps>
  exact?: boolean
}

export const routes: RoutesConfigTypes[] = [
  {
    path: '/',
    component: MainPage,
    exact: true,
  },
];
