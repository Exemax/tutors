import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { routes, RoutesConfigTypes } from './routesConfig';

const RouteRender = ({
  path,
  component,
  exact,
}: RoutesConfigTypes) => {
  if (typeof component === 'string') return null;

  return (
    <Route
      path={path}
      component={component}
      exact={exact}
    />
  );
};

const RoutesApp = () => {
  return (
    <Switch>
      {routes.map((route: RoutesConfigTypes) => {
        return (
          <RouteRender
            key={route.path}
            path={route.path}
            component={route.component}
            exact={route.exact}
          />
        );
      })}
    </Switch>
  );
};

export default RoutesApp;
