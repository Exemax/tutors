import styled from 'styled-components/macro';

export const DefaultButton = styled.button`
  display: block;
  background-color: transparent;
  border: none;
  font-size: 4em;
  height: fit-content;
  :focus {
    outline: none;
  }
`;
