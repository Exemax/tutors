import React from 'react';
import {
  BrowserRouter as Router,
} from 'react-router-dom';
import RoutesApp from './routes/RoutesApp';

const App = () => {
  return (
    <Router>
      <RoutesApp />
    </Router>
  );
};

export default App;
