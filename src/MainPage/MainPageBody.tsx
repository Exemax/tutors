import React, { useState } from 'react';
import styled from 'styled-components/macro';
import { DefaultButton } from '../features/styles/styledComponents';
import { Layout } from '../Layout/LayoutApp';

interface MainPageBodyProps {
  count: number
  onCount: (amount: number) => void
}

const MainPageBody = ({
  count,
  onCount,
}: MainPageBodyProps) => {
  const [amount, setAmount] = useState<number>(0);

  const changeAmount = (e: React.ChangeEvent<HTMLInputElement>) => {
    setAmount(Number(e.target.value));
  };

  return (
    <Layout>
      <Content>
        <Title>Redux toolkit</Title>
        <Section>
          <ViewNumber>{count}</ViewNumber>
          <Buttons>
            <AddCountButton onClick={() => onCount(count + amount)}>+</AddCountButton>
            <SubtractCountButton onClick={() => onCount(count - amount)}>-</SubtractCountButton>
          </Buttons>
          <Input type="number" value={amount} onChange={changeAmount} min={0} />
        </Section>
      </Content>
    </Layout>
  );
};

export default MainPageBody;

const Title = styled.h1`
  font-size: 3em;
  margin-bottom: 32px;
`;

const Content = styled.div`
  
`;

const Section = styled.div`
  background-color: #FFF;
  border-radius: 16px;
  padding: 16px;
`;

const Buttons = styled.div`
  display: flex;
`;

const ViewNumber = styled.span`
  font-size: 4em;
`;

const Input = styled.input`
  border: 1px solid #000;
  border-radius: 8px;
  height: 56px;
  display: flex;
  align-items: center;
  padding: 0 16px;

  :focus {
    outline: none;
    box-shadow: 2px 2px 4px rgba(184, 184, 184, 0.8);
  }
`;

const AddCountButton = styled(DefaultButton)`
  text-shadow: 4px 4px 2px rgba(184, 184, 184, 0.631);
  transition: .1s linear text-shadow;
  :hover {
    text-shadow: 4px 4px 4px rgba(184, 184, 184, 0.8);
  }
  :active {
    text-shadow: 0px 0px 2px rgba(184, 184, 184, 0.631);
  }
`;

const SubtractCountButton = styled(AddCountButton)``;
