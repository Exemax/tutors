import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { counter, selectCount } from '../features/counter';
import MainPageBody from './MainPageBody';

const MainPageApp = () => {
  const count = useSelector(selectCount);

  const dispatch = useDispatch();

  const onAddCount = (amount: number) => {
    dispatch(counter(amount));
  };

  return (
    <MainPageBody
      count={count}
      onCount={onAddCount}
    />
  );
};

export { MainPageApp as MainPage };
