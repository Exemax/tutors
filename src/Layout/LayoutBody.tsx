import React from 'react';
import styled from 'styled-components/macro';
import { LayoutProps } from './LayoutApp';

const LayoutBody = ({
  children,
}: LayoutProps) => {
  return (
    <Page>
      <Header>
        <Logo>&#9851; Tutors</Logo>
      </Header>
      <MainContent>
        {children}
      </MainContent>
      <Footer>
        <Copyright>Exemax tutors corporation</Copyright>
      </Footer>
    </Page>
  );
};

export default LayoutBody;

const Page = styled.div`
  min-height: 100vh;
  background-color: #ebdfdf;
  display: flex;
  flex-direction: column;
`;

const Header = styled.header`
  height: 80px;
  width: 100%;
  padding: 24px;
  background-color: #9C9C9C;
`;

const Footer = styled.footer`
  width: 100%;
  padding: 24px;
  background-color: #3f3f3f;
`;

const MainContent = styled.main`
  flex-grow: 1;
  padding: 24px;
`;

const Logo = styled.h1`
  font-size: 2em;
  
`;

const Copyright = styled.span`
  color: #F1F1F1;
`;
