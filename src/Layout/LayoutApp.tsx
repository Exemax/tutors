import React, { ReactChild } from 'react';
import LayoutBody from './LayoutBody';

export interface LayoutProps {
  children: ReactChild
}

const LayoutApp = ({
  children,
}: LayoutProps) => {
  return (
    <LayoutBody>
      {children}
    </LayoutBody>
  );
};

export { LayoutApp as Layout };
